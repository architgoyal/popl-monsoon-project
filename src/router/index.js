import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: {
            name: 'NFA'
        }
    },
    {
        path: '/nfa',
        name: 'NFA',
        component: () => import('../views/NFA.vue'),
    },
    {
        path: '/regex2nfa',
        name: 'Regex2NFA',
        component: () => import('../views/Regex2NFA.vue'),
    },
    {
        path: '/nfa2dfa',
        name: 'NFA2DFA',
        component: () => import('../views/NFA2DFA.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
